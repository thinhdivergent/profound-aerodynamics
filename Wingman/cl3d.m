function y = cl3d(M,lambda,KL,AR,alpha_zero_lift,alpha)
y = cla3d(M,lambda,KL,AR)*(alpha-alpha_zero_lift)*pi/180;
disp('Lift coefficient');
disp(y);
end