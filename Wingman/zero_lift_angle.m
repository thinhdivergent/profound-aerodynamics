% Nhap vao 4 chu so NACA, xuat ra zero lift angle
function y = zero_lift_angle(M,P,X)
disp('Type of airfoil: NACA')
disp([M,P,X])
M=M/100;
P=P/10;
X=X/100;
syms x;
p = acos(1-2*P);
%f_int = (2*M/(P^2))*(P-0.5+0.5*cos(x))*(1-cos(x));
%a_int = M/((1-P)^2)*(1-2*P+P*(1-cos(x))-0.25*(1-cos(x))^2)*(1-cos(x));
front_int = int((2*M/(P^2))*(P-0.5+0.5*cos(x))*(1-cos(x)),0,p);
aft_int = int(M/((1-P)^2)*(1-2*P+P*(1-cos(x))-0.25*(1-cos(x))^2)*(1-cos(x)),p,pi);
sum_int = double(front_int + aft_int);
disp('Zero lift angle in radian')
disp(-sum_int)
disp('Zero lift angle in degree')
disp(-sum_int/pi*180)
y=-sum_int/pi*180;
end