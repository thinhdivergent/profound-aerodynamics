%TR: taper ratio - for rectangular and trapezoidal wing. TR=0: elliptical
%wing. M: mach number, if M<0.3, incompressible flow is assumed. Lambda:
%swept angle (if wing is swept). If lambda is 0, wing is straight.
function y = cla3d(M,lambda,KL,AR)
a0=2*pi;
lambda=lambda*pi/180; % Convert to radian

if M>0.3
    % Compressible flow
    if lambda==0
        a0=a0/(sqrt(1-M^2));
    else
        a0=a0/(sqrt(1-(M*cos(lambda))^2));
    end
end

if lambda==0
    % Straight wing
    a=a0/((1+a0/(pi*AR))*(1+KL));
else
    % Swept wing
    a0=a0*cos(lambda);
    a=a0/((1+a0/(pi*AR))*(1+KL));
end

disp('Lift slope is');
disp(a);

y=a;

end