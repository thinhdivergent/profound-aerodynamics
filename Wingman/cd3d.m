function y = cd3d(M,cl,cd0,KD,AR)
if(M<0.3)
    % Incompressible flow
    y = cd0 + cl^2*(1+KD)/(pi*AR);
else
    cd0=cd0/sqrt(1-M^2);
    y = cd0 + cl^2*(1+KD)/(pi*AR);
end
disp('Drag coefficient');
disp(y);
end